<?php

declare(strict_types=1);

include 'autoloader.php';

use WebCalMan\Controller\IndexController;

$indexController = new IndexController();
$indexController->run();
