<?php

declare(strict_types=1);

namespace WebCalMan\Controller;

use WebCalMan\Registry;
use WebCalMan\Controller\AbstractController;
use WebCalMan\Mapper\AbstractMapper;
use WebCalMan\Config\IniConfigReader;
use InvalidArgumentException;
use Exception;

/**
 * Контроллер для index.php
 */

class IndexController extends AbstractController
{
    /** @psalm-suppress PropertyNotSetInConstructor */
    private Registry $reg;
    /** @psalm-suppress PropertyNotSetInConstructor */
    private AbstractMapper $mapper;
    /**
     * Получить реестр и маппера.
     */
    public function __construct()
    {
        try {
            $this->reg = Registry::getInstance();
            $this->reg->setConfig(new IniConfigReader(__DIR__ . '/../config.ini'));
            $this->mapper = $this->reg->getMapper();
        } catch (Exception $e) {
            $this->renderError('inicialization of Registry and mapper', $e);
        }
    }
    /**
     * Главный метод для вызова контроллера.
     */
    public function run(): void
    {
        /**
         * Если в передан гет-параметр 'action', значит на страницу нас
         * отправила форма, значит надо её обработать.
         *
         * Соответственно, если совпадёт действие, то вызвать соотв. метод.
         */
        if (isset($_GET['action'])) {
            try {
                if ($_GET['action'] === Registry::SET_ACTION) {
                    $this->save();
                } elseif ($_GET['action'] === Registry::DELETE_ACTION) {
                    $this->delete();
                } elseif ($_GET['action'] === Registry::MODIFY_ACTION) {
                    $this->modify();
                }
            } catch (Exception $e) {
                $this->renderError('saving to log', $e);
            }
            $this->clearUrlFromGetParameners();
        }
        $this->render();
    }
    /**
     * Сохраняет данные, сначала идёт их проверка.
     */
    private function save(): void
    {
        if (
            empty($_GET['prodName']) ||
            empty($_GET['prodWeight']) ||
            empty($_GET['prodCalContent'])
        ) {
            throw new InvalidArgumentException('ERROR: you **must** pass all arguments;');
        }
        $this->mapper->set(
            trim((string) $_GET['prodName']),
            (float) $_GET['prodWeight'],
            (float) $_GET['prodCalContent'],
            (float) $_GET['prodCalContent'] * (float) $_GET['prodWeight'] / 100.0
        );
    }
    /**
     * Удаление строки.
     */
    private function delete(): void
    {
        if (!isset($_GET['id'])) {
            throw new InvalidArgumentException('ERROR: you **must** pass all arguments, no ID given;');
        }
        echo 'true';
        $this->mapper->delete((int) $_GET['id']);
    }
    /**
     * Изменение строки.
     */
    private function modify(): void
    {
        if (
            empty($_GET['prodName']) ||
            empty($_GET['prodWeight']) ||
            empty($_GET['prodCalContent']) ||
            !isset($_GET['id'])
        ) {
            throw new InvalidArgumentException('ERROR: you **must** pass all arguments;');
        }
        $this->mapper->set(
            trim((string) $_GET['prodName']),
            (float) $_GET['prodWeight'],
            (float) $_GET['prodCalContent'],
            (float) $_GET['prodCalContent'] * (float) $_GET['prodWeight'] / 100.0,
            (int) $_GET['id']
        );
    }
    /**
     * Рендер просто создаёт переменну и наполняет её. Тут больше html.
     */
    public function render(): void
    {
        $resultTable = '';
        /**
         * @var array<string, string> $arrayOfFood
         * @var string $mainArrayKey
         */
        foreach ($this->mapper->get(-1) as $mainArrayKey => $arrayOfFood) {
            $resultTable .= '<tr> <form method="get" action="' . Registry::FORM_ACTION . '">';
            foreach ($arrayOfFood as $subArray => $foodProperty) {
                $resultTable .= '<td> <input type="text" name="' . $subArray . '" value="'  . $foodProperty . '"></td>';
            }

            $resultTable .= '
                <td><input type="hidden" value="' . Registry::MODIFY_ACTION . '" name="action"><input type="hidden" name="id" value="' . $mainArrayKey . '"><input type="submit" value="@"> </form></td>
                <td>
                <form action = "' . Registry::FORM_ACTION . '" method="get">
                    <input type="hidden" value="' . Registry::DELETE_ACTION . '" name = "action">
                    <input type="hidden" value="' . $mainArrayKey . '" name="id">
                    <input type="submit" value="X"> </form>
            </td>
            </tr>';
        }
        include __DIR__ . '/../View/IndexView.php';
    }
    public function renderError(string $process, Exception $e): void
    {
        printf(
            '<b>An error occured during ' . $process . '!</b>' .
            '<br><br>' .
            'Information:' .
            '<br>' .
            '<code>' .
            $e->__toString() .
            '</code>'
        );
         exit(1);
    }
}
