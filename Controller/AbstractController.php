<?php

namespace WebCalMan\Controller;

abstract class AbstractController
{
    /**
     * Очистка адреса от гет-параметров
     */
    protected function clearUrlFromGetParameners(): void
    {
        $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = strtok($url, '?');
        header('Location: ' . $url);
    }
    abstract public function run(): void;
}
