<?php

namespace WebCalMan;

use WebCalMan\Config\AbstractConfigReader;
use WebCalMan\Mapper\AbstractMapper;
use InvalidArgumentException;
use RuntimeException;

/**
 * Реестр предоставляет единый класс для хранения информации
 *
 */
class Registry
{
    /** @var array<string, string> */
    private array $config = [];
    /**
      * @psalm-suppress PropertyNotSetInConstructor
      *
      * @var AbstractMapper | null
      */
    private $mapper = null;
    /** @var self|null */
    private static $registry = null;
    public const FORM_ACTION = 'index.php';
    public const PRODUCT_NAME = 'prodName';
    public const PRODUCT_WEIGHT = 'prodWeight';
    public const SET_ACTION = 'set';
    public const DELETE_ACTION = 'delete';
    public const MODIFY_ACTION = 'modify';
    public const PRODUCT_CALORIE_CONTENT = 'prodCalContent';

    /**
     * Закрытый конструктор --- нельзя создавать экземпляры при помощи new.
     */
    private function __construct()
    {
    }
    /**
     * Получение единого экземпляра объекта, хранящегося в статической переменной. 
     * Если его нет --- создать.
     *
     * @return self
     */
    public static function getInstance(): self
    {
        if (is_null(self::$registry)) {
            self::$registry = new self();
        }
        return self::$registry;
    }
    /**
     * Получить читателя конфига, прочитать конфиг и распарсить его.
     *
     * @param AbstractConfigReader $confiReader
     * @return void
     */
    public function setConfig(AbstractConfigReader $confiReader): void
    {
        $confiReader->readConfig();
        $this->config = $confiReader->getConfigOptions();
        $this->parseConfig();
    }
    /**
     * Совершает дополнительные операции, которые необходимо совершить над опциями конфигурации.
     *
     * @return void
     */
    private function parseConfig(): void
    {
        /**
         * Если существует параметр 'jsonlog.json', то проверить его первый символ --- если
         * он --- разделитель директорий, то путь абсолютный, его не трогать, если же нет
         * то путь относительный, добавить к нему текущую директорию, сделав абсолютным.
         */
        if (isset($this->getConfig()['jsonlog.json'])) {
            if ($this->getConfig()['jsonlog.json'][0] !== DIRECTORY_SEPARATOR) {
                $this->config['jsonlog.json'] = __DIR__ . DIRECTORY_SEPARATOR . $this->config['jsonlog.json'];
            }
        }
    }
    /**
     * Получить конфиг. 
     *
     * @return array<string, string>
     * */
    public function getConfig(): array
    {
        return $this->config;
    }
    /**
     * Получить датамаппера, он реализует простейший высокоуровневый интерфейс для работы с данными.
     *
     * Маппер создаётся, используя параметр 'mapper' в конфигурации.
     *
     * @return AbstractMapper
     */
    public function getMapper(): AbstractMapper
    {
        if ($this->mapper === null) {
            if (empty($this->getConfig())) {
                throw new RuntimeException('ERROR: could not create mapper: no config;');
            }
            if (!isset($this->getConfig()['mapper'])) {
                throw new RuntimeException('ERROR: there is no config optio for mapper;');
            }
            $fullMapperName = 'WebCalMan\\Mapper\\' . $this->getConfig()['mapper'];
            if (!class_exists($fullMapperName)) {
                throw new InvalidArgumentException('ERROR: cannot create mapper class;');
            }
            $this->mapper = new $fullMapperName();
            if (!($this->mapper instanceof AbstractMapper)) {
                throw new InvalidArgumentException('ERROR: resulting mapper class is not instance of AbstractMapper;');
            }
        }

        return $this->mapper;
    }
}
