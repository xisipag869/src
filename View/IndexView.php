<?php

/**
 * Получить реестр.
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . '../Registry.php';
use WebCalMan\Registry;

/**
 * Если переменное не существует, то выбросить исключение. Это преимущественно для пхпстана.
 */

if (!isset($resultTable)) {
    throw new RuntimeException('ERROR: there is no resultTable variable to display;');
}

?>

<head>
    <style>
        table, td, th{
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
            text-align: center;
        }
    </style>
</head>
<body>
<form method='get' action='<?=Registry::FORM_ACTION;?>'>
<input type='text' name='<?=Registry::PRODUCT_NAME;?>' placeholder='Name of the product'>
<br><br>
<input type='number' name='<?=Registry::PRODUCT_WEIGHT;?>' placeholder='Weight of the product'>
<br><br>
<input type='number' name='<?=Registry::PRODUCT_CALORIE_CONTENT;?>' placeholder='Calorie content in product per 100g'>
<br><br>
    <input type='hidden' value='<?=Registry::SET_ACTION;?>' name='action'>
<input type='submit' value='submit' name='submit'>
</form>

<table>
    <tr>
        <th> Product name </th>
        <th> Product weight </th> 
        <th> Product calorie content per 100g</th> 
        <th> Product calorie content per portion </th>
        <th> MODIFY </th>
        <th> DELETE </th>
</tr> 
<?= $resultTable; ?>
</table>
<body>
