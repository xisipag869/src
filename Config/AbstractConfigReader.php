<?php

namespace WebCalMan\Config;

use RuntimeException;

/**
 * Читает конфиг.
 */

abstract class AbstractConfigReader
{
    protected string $configFilePath;
    /** @var array<string, string> */
    protected array $configOptions = [];
    /** @trows RuntimeException if file does not exist */
    final public function __construct(string $configFilePath)
    {
        if (!file_exists($configFilePath)) {
            throw new RuntimeException('ERRROR: config file does not exist;');
        }
        $this->configFilePath = $configFilePath;
        $this->readConfig();
    }
    /** @throws RuntimeException if config file contains an error; */
    abstract public function readConfig(): void;
    final public function getConfigFilePath(): string
    {
        return $this->configFilePath;
    }
    /** @return array<string, string> */
    final public function getConfigOptions(): array
    {
        return $this->configOptions;
    }
}
