<?php

declare(strict_types=1);

namespace WebCalMan\Config;

use WebCalMan\Config\AbstractConfigReader;
use RuntimeException;

class IniConfigReader extends AbstractConfigReader
{
    /**
     * Просто читает конфиг и заносит его в свойство.
     */
    public function readConfig(): void
    {
        /** @var array<string, string> | bool */
        $tmp = parse_ini_file($this->configFilePath);
        if (!is_array($tmp)) {
            throw new RuntimeException('ERROR: error occured during config file parsing;');
        }
        $this->configOptions = $tmp;
    }
}
