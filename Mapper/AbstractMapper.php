<?php

namespace WebCalMan\Mapper;

use WebCalMan\Registry;
use RuntimeException;
use InvalidArgumentException;

abstract class AbstractMapper
{
    protected Registry $reg;
    final public function __construct()
    {
        $this->reg = Registry::getInstance();
    }
    /**
      *  @return array<int|string, string|float|array>
      *
      * @throws RuntimeException
      * @throws InvalidArgumentException if row with @param $idOfRow  does not
      * exist;
      */
    abstract public function get(int $idOfRow): array;
    /**
      * @param string $prodName name of the product
      * @param float $prodWeight weight of the product
      * @param float $prodCalContent calorie content in that product per 100 grams
      * @param float $portionCalContent calorie content in current portion
      * @param int $position describs position of array to set (default = -1,
      * another values for modifying)
      */
    abstract public function set(
        string $prodName,
        float $prodWeight,
        float $prodCalContent,
        float $portionCalContent,
        int $position = -1
    ): void;
    abstract public function delete(int $idOfRow): void;
}
