<?php

namespace WebCalMan\Mapper;

use WebCalMan\Mapper\AbstractMapper;
use RuntimeException;
use InvalidArgumentException;

/**
 * Простенький маппер для работы с жсон-файлом, получаемым из реестра
 */

class JsonMapper extends AbstractMapper
{
    /**
     * Получить строку или все строки (если номер строки === -1).
     */
    public function get(int $idOfRow): array
    {
        if (!is_readable($this->reg->getConfig()['jsonlog.json'])) {
            throw new RuntimeException('ERROR: log file does not exist or it is not readable;');
        }
        $fileTMP = file_get_contents($this->reg->getConfig()['jsonlog.json']);
        if ($fileTMP === false) {
            throw new RuntimeException('ERROR: parsing config file returned false;');
        }
        /** @var array<int, array<int, string|float>>|bool|null */
        $tmp = json_decode($fileTMP, true);
        if ($tmp === null) {
            throw new RuntimeException('ERROR: cannot parse log file;');
        } elseif (!is_array($tmp)) {
            throw new RuntimeException('ERRROR: during parsing log file;');
        }

        if ($idOfRow === - 1) {
            return $tmp;
        } elseif (!isset($tmp[$idOfRow])) {
            throw new InvalidArgumentException('ERROR: there is no such row in log;');
        }
        return $tmp[$idOfRow];
    }
    /**
     * Установить строку (если не указан  её номер), или изменить её.
     * (строка --- массив, просто термин из реляционной БД)
     */
    public function set(
        string $prodName,
        float $prodWeight,
        float $prodCalContent,
        float $portionCalContent,
        int $position = -1
    ): void {
        /**
         * Факл именуетя логом, переменная лог хранит весь файл, работа
         * продолжается с ней
         */
        $log = $this->get(-1);
        if ($position === -1) {
            $log[] = [
                'prodName' => $prodName,
                'prodWeight' => $prodWeight,
                'prodCalContent' => $prodCalContent,
                'portionCalContent' => $portionCalContent
            ];
        } else {
            $log[$position] = [
                'prodName' => $prodName,
                'prodWeight' => $prodWeight,
                'prodCalContent' => $prodCalContent,
                'portionCalContent' => $portionCalContent
            ];
        }
        if (!is_writable($this->reg->getConfig()['jsonlog.json'])) {
            throw new RuntimeException('ERROR: log file is not writtable;');
        }
            file_put_contents($this->reg->getConfig()['jsonlog.json'], json_encode($log));
    }
    /**
     * Удаление одной строки
     */
    public function delete(int $idOfRow): void
    {
        $result = [];
        foreach ($this->get(-1) as $key => $val) {
            if ($key === $idOfRow) {
                continue;
            }
            $result[] = $val;
        }
        if (!is_writable($this->reg->getConfig()['jsonlog.json'])) {
            throw new RuntimeException('ERROR: log file is not writtable;');
        }
        file_put_contents($this->reg->getConfig()['jsonlog.json'], json_encode($result));
    }
}
