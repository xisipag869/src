<?php

/**
 * Autoloader.
 *
 * After Registrying this autoload function with SPL, the following line
 * would cause the function to attempt to load the \Foo\Bar\Baz\Qux class
 * from /path/to/project/src/Baz/Qux.php:
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */

declare(strict_types=1);

spl_autoload_register(function (string $class) {
    $prefix = 'WebCalMan\\';
    $base_dir = __DIR__ . DIRECTORY_SEPARATOR;
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});
